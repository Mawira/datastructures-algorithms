﻿using DSA.DataStructureAndAlgorithms.Arrays;
using DSA.DataStructureAndAlgorithms.LinkedList;
using DSA.DataStructureAndAlgorithms.HashTable;
using DSA.DataStructureAndAlgorithms.Strings;
using DSA.DataStructureAndAlgorithms.Stacks;
using DSA.DataStructureAndAlgorithms.Queues;
using DSA.DataStructureAndAlgorithms.Trees;

using System;
using System.Collections.Generic;
using DSA.DataStructureAndAlgorithms.Trees.AVLTree;
using DSA.DataStructureAndAlgorithms.Problems;
using System.ComponentModel.Design;

namespace DSA
{
    public class Program
    {
        static void Main(string[] args)
        {
            // var r = new RecursionMtd();
            //Console.WriteLine(r.Fib(5));
            //  r.PrintCount();
            //var list = new List<int> { 1, 3, 4, 4, 6 };
            //Console.WriteLine("myList"+list.Count);
            //Console.WriteLine("myList" + list[0]);
            //Dictionary<int, int> dic = new Dictionary<int, int>();
            //foreach (var item in list)
            //{
            //    if (dic.ContainsKey(item))
            //    {
            //        dic[item]++;
            //    }
            //    else
            //    {
            //        dic.Add(item, 1);
            //    }
            //}

            // Console.WriteLine("my value" + dic[4]);
            //var t =new TwoDimArray();
            //t.MissingInteger();
            //var list2 = new CustomLinkedList();
            //list2.AddLast(90);
            //list2.AddLast(23);
            //list2.AddLast(70);
            //list2.AddLast(53);
            //list2.AddLast(40);
            //list2.AddLast(13);

            //list2.Shift(2);
            ///list2.Shift1(-2);
            // //
            // list2.AddFirst(79);
            //// list2.Print();
            // Console.WriteLine(">>>>Reversed<<<<");
            //// list2.Reverse();
            //var list1 = new CustomLinkedList();

            //list1.AddLast(90);
            //list1.AddLast(23);
            //list1.AddLast(70);
            //list1.AddLast(53);
            //list1.AddLast(40);
            //list1.AddLast(13);
            ////
            //list1.AddFirst(79);
            //list1.Print();



            // var list = new CustomLinkedList();

            //   list.AddLast(4);
            //   list.AddLast(3);
            //   list.AddLast(4);
            //   list.AddLast(1);
            //   list.AddLast(3);
            //   list.AddLast(7);
            ////   var head = list.AddLast(7);
            //  var head1 = list.ReverseUsingRecursion(head);


            // var h = new CustomHashTable(8);
            //h.LinearBrobinInsert("peter");
            //h.LinearBrobinInsert("peterm");
            //h.LinearBrobinInsert("peter");
            //h.LinearBrobinInsert("petern");
            // h.Find("peter");
            // h.PrintHT();
            //list.ReverseInPlace();
            //var f = new RecursionMtd();
            //f.factorial(1);
            // Console.WriteLine(f.isPrime(9));
            // Console.WriteLine(f.isPrime(9));
            //list.SortLinkdList();
            // list.RemoveDuplicate();
            //Console.WriteLine(list.GetMiddleElement());
            // list.ReverseInPlace();
            //p.AddTwoList(list.Head, list1.Head);

            //Console.WriteLine("index of" + list.IndexOf(900));
            //Console.WriteLine(list.Contains(900));
            // int [] A = new int[] { 3, 4, 2, 1, 5 };
            // int lb = 0;
            //int  ub = A.Length - 1;
            //  var arr = new ArrayOperations();


            //  foreach (var item in arr.QuickSort(A, lb, ub))
            //  {
            //      Console.WriteLine(item);
            //  }
            // arr.Print();
            //Console.WriteLine(arr.TripleSum());
            //Console.WriteLine(arr.SubSetSum());
            //  Console.WriteLine(arr.EqualizeByDivisionMin());
            // arr.EqualizeArray();
            //arr.SortArray();
            //Console.WriteLine(arr.GetMajority());
            // arr.ReverseNumber(45367);
            // Console.WriteLine(arr.GetTallest());
            // var d = new AABB();
            // Console.WriteLine(d.Compress());
            //  Console.WriteLine(d.MinRemoval());

            // Console.WriteLine(arr.isValidSubSequence());

            // var stack = new BasicStack();
            //stack.Insert(34);
            //stack.Insert(4);
            //stack.Insert(94);
            //stack.Insert(39);

            //stack.RemovePop();
            //stack.RemovePop();
            //stack.Push(2);
            //stack.Push(2);
            //stack.Push(2);
            //stack.Push(2);
            //stack.Push(3);
            // stack.Push(3);


            //Console.WriteLine("Before pop");
            //stack.Print();
            //stack.Pop();
            //Console.WriteLine("After pop");
            //stack.Print();

            //var ht = new HTSeparateChaining<int>(5);
            //ht.AddItem("Peter", 8);
            //ht.AddItem("nelsonmawira", 8);
            //ht.AddItem("Petez", 9);
            //ht.AddItem("Petet", 10);
            // ht.Get("Petez");
            //ht.Remove("Petez");
            //var s = new MyString();
            //s.IsPalindrome("mom");

            //var q = new CustomQ(4);
            //q.Enqueue(2);
            //q.Enqueue(4);
            //q.Enqueue(6);
            //q.Enqueue(7);

            //q.Dequeue();
            //q.Dequeue();

            //var bt = new BTree();
            ///  var bt = new BTree();

            //var bt = new AVLTree();
            //bt.Insert(3);
            //bt.Insert(9);
            //bt.Insert(20);
            //bt.Insert(15);
            //bt.Insert(7);


            //Console.WriteLine("leaf nodes");
            // bt.NumberOfLeafNode();
            //Console.WriteLine("tree height");
            //bt.HeightOfBT();

            //bt.GetSmallest();
            //bt.GetBiggest();
            //bt.RemoveNode(90);
            //bt.SoftDelete(90);

            //bt.InOrderTraversal();
            //bt.PreOrderTraversal();
            //bt.PostOrderTraversal();



            // Console.WriteLine(bt.FindRecursive(4));
            //var s = new MyString();
            //var head = s.SendChars();

            //s.Compress();
            //s.Decompress();
            //  s.ConvertDecimalToBinary();
            //s.ReverseInOrder();
            //var ar = new TwoDimArray();
            //  ar.CheckIfSumExist();
            // ar.PopulateTwoDimArray();
            //Console.WriteLine(s.RemoveDu("free"));
            //int[] A = { 6,3,4,8};
            //ArrayOperations a = new ArrayOperations();
            //Console.WriteLine(a.GetMinMissingNumGreaterThanZero(A));
            //  a.ReverseArrayInMemory();
            //a.Print();
            //LargestSum s = new LargestSum();
            //Console.WriteLine(s.Sum());
            //s.Reverse();
            // int quotient = 0;
            //int num1 = 18;
            //int num2 = 4;
            // while (num1 >= num2)
            // {
            //     num1 = num1 - num2;
            //     quotient++;
            // }
            // Console.WriteLine("jjjjjj");
            // Console.WriteLine(quotient);
            //int N = 32;

            //int k = 0;

            //List<int> binaryNums = new List<int>();

            //while (N > 0)
            //{

            //    int rem = N % 2;
            //    N = N / 2;
            //    binaryNums.Add(rem);
            //    k++;
            //}

            //foreach (var item in binaryNums)
            //{
            //    Console.Write(item);
            //}

            //int zerosCount = 0;
            //int zeroMaxCount = 0;

            //for (int i = 0; i < binaryNums.Count ; i++)
            //{
            //    int p = binaryNums[i];

            //    if ( binaryNums[i] == 0)
            //    {
            //        zerosCount++;
            //        continue;
            //    }
            //    zeroMaxCount = zeroMaxCount > zerosCount ? zeroMaxCount : zerosCount;
            //    zerosCount = 0;

            //}
            //Console.WriteLine(zeroMaxCount);
            //Console.ReadLine();

            //var frogs = new Frogs();
            //frogs.Solution();

            //BST

            //https://www.youtube.com/watch?v=pN1RWeX47tg
            //https://www.youtube.com/watch?v=CrCClRcLTfU
            //AVI Tree  https://simpledevcode.wordpress.com/2014/09/16/avl-tree-in-c/
            Solution();
        }

        public static void Solution()
        {
            Console.WriteLine("Testing....");
        }
    }
}
