﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Problems
{
    public class Soldiers
    {
        public int Solution()
        {
            // var ranks = new int[] { 3,4,3,0,2,2,3,0,0};
            //var sorted = ranksArray.OrderBy(x => x).ToArray();
            var ranks = new int[] { 4, 4, 3, 3, 1, 0 };
            Array.Sort(ranks);
            //0,0,0,2,2,3,3,3,4
            int count = 0;
            int previous = 0;
            int lowerBoundery = 0;
            for (int i = 1; i < ranks.Length; i++)
            {
                int temp1 = ranks[previous];
                int temp2 = ranks[i];
                if (ranks[previous] == ranks[i])
                {
                    continue;
                }

                if (ranks[previous] + 1 == ranks[i])
                {
                    count += i - lowerBoundery;
                    lowerBoundery = i;
                }
                else
                {
                    lowerBoundery = i;
                }
                previous = i;
            }

            return count;
        }
    }
}
