﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Problems
{
    public class AABB
    {
        public class Node
        {
            public int Count { get; set; }
            public char Value { get; set; }
            public Node Next { get; set; }

            public Node(char Value, int Count, Node Next = null)
            {
                this.Count = Count;
                this.Value = Value;
                this.Next = Next;
            }
        }

        public Node Root { get; set; }

        public Node Compress()
        {
            string s = "bcaabddeafee";
            char[] input = s.ToCharArray();
            int len = input.Length;
            int counter = 1;
            for (int i = 0; i < len; i++)
            {
              
                if (i + 1 <= len - 1 && s[i] == s[i + 1])
                {
                    counter++;
                    continue;
                }
                else
                {
                    Node node = new Node(s[i], counter);
                    if (Root == null)
                    {
                        Root = node;
                       
                    }
                    else
                    {
                         SendForCompression(Root, node);
                    }
                    counter = 1;
                }
            }
            return Root;
        }

        public Node SendForCompression(Node root, Node node)
        {
            Node current = root;

            while(current.Next != null)
            {
                current = current.Next;
            }

            current.Next = node;

            return root;
        }
        public int MinRemoval()
        {
            string S = "AAABBB";
            // char[] s = S.ToCharArray();
            int numberOfAs = 0, numberOfBs = 0, numberOfDeletions = 0, len = S.Length;

            if (string.IsNullOrWhiteSpace(S))
            {
                return 0;
            }

            for (int i = 0; i < len; i++)
            {
                char c = S[i];
                // count number of As in the string

                if (c == 'A')
                {
                    numberOfAs++;
                    if (numberOfBs > numberOfDeletions)
                    {
                        numberOfDeletions++;
                    }
                }
                else
                {
                    numberOfBs++;

                    if (numberOfAs == 0)
                    {
                        numberOfDeletions++;
                    }
                }


            }
            return Min(numberOfAs, numberOfBs, numberOfDeletions);
            //string s = "AAABBB";
            //int cntA = 0, cntB = 0, cntRemove = 0;
            //    for (int i = 0; i<s.Length; i++)
            //    {
            //        char c = s[i];
            //        if (c == 'A')
            //        {
            //            cntA++;
            //            if (cntB > cntRemove)
            //                cntRemove++;
            //        }
            //        else
            //        {
            //            cntB++;
            //            if (cntA == 0)
            //                cntRemove++;
            //        }
            //    }
            //    return cntRemove;

            //int numB = 0, removal = 0;

            //int len = s.Length;

            //for (int i = 0; i < len; i++)
            //{
            //    char c = s[i];
            //    if(c == 'A')
            //    {
            //        //minimum deletions with this character included
            //        //is equal to count of all Bs before it
            //        removal = Math.Min(removal + 1, numB);
            //    }
            //    else
            //    {
            //        // there is no need to exclude the last B at the end of  
            //        // the string, the min_dels does not change
            //        numB++;
            //    }
            //}
            //return removal;
        }

        private int Min(int a, int b, int c)
        {
            int d = b < c ? b : c;
            return a < d ? a : d;
        }
    }
}
