﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Problems
{
    // https://www.csharpstar.com/csharp-algorithms/
    //https://www.youtube.com/watch?v=4LvYp_d6Ydg
    //https://www.youtube.com/watch?v=cETfFsSTGJI
    public class Frogs
    {
        public void Solution()
        {
            var blocks = new int[] { 2,6,8,5 };

            var distances = new List<int>();

            for (int i = 0; i < blocks.Length; i++)
            {
                int rightIndex = GoRight(blocks, i);
                int leftIndex = GoLeft(blocks, i);

                distances.Add(rightIndex - leftIndex + 1);
            }

            Console.WriteLine(distances.Max());
        }

        public int GoRight(int[] blocks, int startIndex)
        {
            int rightIndex = 0;

            for (int i = startIndex + 1; i < blocks.Length; i++)
            {
                if (blocks[rightIndex] <= blocks[i])
                {
                    rightIndex++;
                    startIndex++;
                }

                if(blocks[startIndex] > blocks[i])
                {
                    break;
                }
            }
            return rightIndex;
        }
        public int GoLeft(int[] blocks, int startIndex)
        {
            int leftIndex = startIndex;

            if (startIndex == 0)
            {
                return 0;
            }

            for (int i = startIndex; i <= 0; i--)
            {
                if (blocks[startIndex] <= blocks[i])
                {
                    startIndex = i;
                }
                if(blocks[startIndex] > blocks[i])
                {
                    break;
                }
            }

            return leftIndex;
        }
    }
}
