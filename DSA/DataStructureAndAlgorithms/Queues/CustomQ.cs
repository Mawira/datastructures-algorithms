﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Queues
{
    //queue implementiomn using array   
    public class CustomQ<T>
    {
        public int Size { get; set; }
        public T [] Queue { get; set; }
        public int Rear { get; set; }
        public int Front { get; set; }        

        public CustomQ(int Size)
        {
            this.Size = Size;
            Front = -1;
            Rear = -1;
            Queue = new T[Size];
        }

        public void Enqueue(T item)//O(1)
        {
            if (Rear == -1 && Front == -1)
            {
                Rear = 0;
                Front = 0;
                Queue[Rear] = item;

            }
            else
            {
                Rear++;
                Queue[Rear] = item;
            }

        }
        public T Dequeue()//O(1)
        {
            if (Size == 0)
            {
                throw new InvalidOperationException("Queue is empty.");
            }

            if (Front == Rear)
            {
                Rear = -1;
                Front = -1;
            }
            else
            {
                Front++;
            }

            T item = Queue[Front];
            return item;
        }

        public T Peek()//O(1)
        {
            if (Size == 0)
            {
                throw new InvalidOperationException("Queue is empty.");
            }

            return Queue[Front];
        }

        //impliment using linked list
        public Node Head { get; set; }
        public Node Tail { get; set; }
        public void Insert(int value)
        {
            Node node = new Node(value);

            if (Head == null)
            {
                Head = node;
                Tail = node;
            }
            else
            {
                Head.Next = node;
                Tail = node;
            }
        }

    }
}
