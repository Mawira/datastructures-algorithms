﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Queues
{
    public class Node
    {
        public int Data { get; set; }
        public Node Next { get; set; }
        public Node(int Data, Node Next = null)
        {
            this.Data = Data;
            this.Next = Next;
        }
    }
}
