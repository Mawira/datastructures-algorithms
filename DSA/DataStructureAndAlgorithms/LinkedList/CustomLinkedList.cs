﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
//https://www.geeksforgeeks.org/nth-node-from-the-end-of-a-linked-list/?ref=lbp
//https://medium.com/techie-delight/linked-list-interview-questions-and-practice-problems-55f75302d613
namespace DSA.DataStructureAndAlgorithms.LinkedList
{
    public class CustomLinkedList
    {
        public Node Head { get; set; }
        public Node Tail { get; set; }

        public Node AddLast(int item)
        {
            var node = new Node(item);
            if (Head == null)
            {
                Head = node;
                Tail = node;
            }
            else
            {
                var current = Head;
                while (current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = node;
                Tail = node;

            }
            return Head;
            //var node = new Node(item);
            //if (Head != null)
            //{
            //    Tail.Next = node;
            //    Tail = node;

            //}
            //else
            //{
            //    Head = node;
            //    Tail = node;
            //}
            //return Head;
        }

        public Node AddFirst(int item)
        {
            var node = new Node(item);
            if (Head == null)
            {
                Head = node;
                Tail = node;
            }
            else
            {
                node.Next = Head;
                Head = node;
            }
            return Head;
        }

        public void delete(int data)
        {
            var temp = Head;
            var previous = Head;
            if (Head.Value == data && Tail.Value == data)
                Head = Tail = null;

            if (Tail.Value == data)
                DeleteLast();

            if (Head.Value == data)
                DeleteFirst();

            while (temp != null)
            {
                if (temp.Value == data)
                {
                    previous.Next = temp.Next;
                    temp.Next = null;
                    break;
                }
                previous = temp;
                temp = temp.Next;
            }
            if (temp == null)
            {
                Console.WriteLine("not found");
            }
        }

        public void DeleteFirst()
        {

            if (Head == null)
            {
                Console.WriteLine(-1);
            }
            else
            {
                var second = Head.Next;
                Head.Next = null;
                Head = second;

            }
        }
        public void DeleteLast()
        {

            var current = Head;
            // var l = node.Value;
            var t = current.Next.Value;
            while (current != null)
            {
                if (current.Next == Tail)
                {
                    current.Next = null;
                    Tail = current;
                }
                else
                {
                    current = current.Next;
                }
            }
        }

        public void DeleteAtAposition(int position)
        {
            var previous = Head;
            int index = 0;
            var temp = Head;
            while (temp != null)
            {
                previous = temp;
                temp = temp.Next;
                index++;

                if (position == index)
                {
                    previous.Next = temp.Next;
                    temp.Next = null;
                }
            }
        }

        public int IndexOf(int item)
        {
            int index = 0;
            var temp = Head;
            while (temp != null)
            {
                if (temp.Value == item)
                {
                    return index;
                }
                else
                {
                    temp = temp.Next;
                    index++;
                }
            }
            return -1;
        }

        public void AddTwoList(Node head1, Node head2)
        {
            var temp1 = head1;
            var temp2 = head1;
            var result = new CustomLinkedList();
            while (temp1 != null)
            {
                result.AddLast(temp1.Value + temp2.Value);
                temp2 = temp2.Next;
                temp1 = temp1.Next;
            }
            result.Print();

        }
        public void SumCustomLinkedListElement()
        {
            var temp = Head;
            int sum = 0;
            while (temp != null)
            {
                sum += temp.Value;
                temp = temp.Next;
            }
            Console.WriteLine(sum);
        }

        //reverse in place
        public Node ReverseInPlace()
        {
            Node previous = null;
            Node current = Head;
            // Node temp = null;

            while (current != null)
            {
                Node temp = current.Next;
                current.Next = previous;
                previous = current;
                current = temp;
            }

            Head = previous;
            return Head;
        }
        //reverse using recursion
        // https://www.youtube.com/watch?v=W-EfGB0E_ao
        public Node ReverseUsingRecursion(Node head, Node newHead = null)
        {


            if (head == null)
            {
                return null;
            }
            if (head.Next == null)
            {
                return newHead;
            }

            Node nextNode = head.Next;
            head.Next = newHead;
            newHead = head;
            head = nextNode;
            return ReverseUsingRecursion(head, newHead);
        }
        public bool Contains(int item)
        {
            return IndexOf(item) != -1;
        }

        public Node sort()
        {
            Hashtable hash = new Hashtable();
            var ordered = new CustomLinkedList();

            Node temp = Head;
            while (temp != null)
            {

                hash.Add(temp.Value, 1);
                temp = temp.Next;
            }

            foreach (DictionaryEntry entry in hash)
            {
                ordered.AddLast((int)entry.Key);
            }
            //  ordered.Print();
            ordered.ReverseInPlace();
            return ordered.Head;
        }

        public void SortLinkdList()
        {
            Node current = Head.Next;

            while (current != null)
            {
                Node previous = Head;

                while (previous != current)
                {
                    if (previous.Value > current.Value)
                    {
                        int temp = current.Value;
                        current.Value = previous.Value;
                        previous.Value = temp;
                    }
                    else
                    {
                        previous = previous.Next;
                    }
                }

                current = current.Next;
            }
            Print();
        }

        //remove all element that match a given value x
        public Node RemoveElement(int x)
        {
            Node previous = null;
            Node current = Head;
            Node next = null;


            if (x == Head.Value)
            {
                Node second = Head.Next;
                Head.Next = null;
                Head = second;
                return Head;
            }

            if (x == Tail.Value)
            {

                while (current != null)
                {
                    if (current.Next.Value == x)
                    {
                        current.Next = null;
                    }
                    previous = current;
                    current = current.Next;

                }
                return Head;
            }
            while (current != null)
            {
                if (current.Value == x)
                {
                    previous.Next = current.Next;
                    current = next;
                    next = next.Next;
                }
                next = current.Next;
                previous = current;
                current = next;

            }
            return Head;
        }
        public Node RemoveDuplicate()
        {


            //Node current = head;
            //while (current.next != null)
            //{
            //    // If data is equal, reset the next pointer
            //    if (current.data == (current.next).data)
            //    {
            //        current.next = (current.next).next;
            //    }
            //    else
            //    {
            //        // If data is not equal, go
            //        // to the next element
            //        current = current.next;
            //    }
            //}

            //return head;

            Hashtable hash = new Hashtable();
            Node temp = Head;
            Node previous = null;

            while (temp != null)
            {
                if (!hash.Contains(temp.Value))
                {
                    hash.Add(temp.Value, 1);

                }
                else
                {
                    previous.Next = temp.Next;
                }
                previous = temp;
                temp = temp.Next;
            }
            //var newList = new CustomLinkedList();
            //foreach (DictionaryEntry item in hash)
            //{

            //    newList.AddLast((int)item.Key);

            //}
            //newList.Print();
            // Print();
            return Head;
        }

        public int GetMiddleElement()
        {
            int middleIndex = GetElementsCount() / 2;
            Node temp = Head;

            int count = 0;

            while (temp != null)
            {
                if (count == middleIndex)
                {
                    return temp.Value;
                }
                count++;
                temp = temp.Next;
            }
            return count;
        }

        public int GetElementsCount()
        {
            Node temp = Head;
            int count = 0;
            while (temp != null)
            {
                count++;
                temp = temp.Next;
            }
            return count;
        }
        public void Print()
        {
            var temp = Head;

            while (temp != null)
            {
                Console.WriteLine(temp.Value);
                temp = temp.Next;
            }
        }
        //time complexity O(nk)
        public Node Shift(int k)
        {
            //positive shift > 1->5->3->4->5 k=2
            // 4 -> 5 -> 1 ->5 ->3
            int l = k;
            if (k == 0)
                return Head;
            Node current = Head;
            Node previous = null;
            for (int i = 1; i <= Math.Abs(k); i++)
            {
                if (l > 0)//positive
                {
                    while (current.Next != null)
                    {
                        previous = current;
                        current = current.Next;
                    }
                    Node temp1 = current;
                    current.Next = Head;
                    Head = current;
                    current = temp1;
                    previous.Next = null;
                }
                else
                {
                    Node temp = Head.Next;
                    Tail.Next = Head;
                    Tail = Head;
                    Head.Next = null;
                    Head = temp;
                }

                //Node temp = Head.Next;
                //current.Next = Head;
                //Head.Next = null;
                //Head = temp;

            }

            return Head;
        }

        //time complexity O(n)
        public Node Shift1(int k)
        {
            Node tail = Head;
            int listLength = 0;

            while (tail.Next != null)
            {
                listLength++;
                tail = tail.Next;
            }
            tail.Next = Head;
            Node newTail = Head;

            int shift = Math.Abs(k) % listLength;

            int offSet = k > 0 ? listLength - shift : shift;

            for (int i = 1; i < offSet; i++)
            {
                newTail = newTail.Next;
            }
            Node newHead = newTail.Next;
            newTail.Next = null;

            return newHead;
        }
        //check for cyclic linked list
        public bool CheckCycle(Node head)
        {
            Node current = head;
            HashSet<Node> nodes = new HashSet<Node>();
            while (current != null)
            {

                if (nodes.Contains(current))
                {
                    return true;
                }
                else) { 
                nodes.Add(current);}
                current = current.Next;
            }
            return false;
        }
    }
}
