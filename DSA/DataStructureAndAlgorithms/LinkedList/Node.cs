﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.LinkedList
{
   public class Node
    {
        public int Value { get; set; }
        public Node Next { get; set; }

        public Node(int Value,Node Next = null)
        {
            this.Value = Value;
            this.Next = Next;
        }
    }
}
