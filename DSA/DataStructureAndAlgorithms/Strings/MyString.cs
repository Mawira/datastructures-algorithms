﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Strings
{
    public class Node
    {
        public int Count { get; set; }
        public char value { get; set; }
        public Node Next { get; set; }
        public Node( char value, int Count, Node Next = null)
        {
            this.Count = Count;
            this.value = value;
            this.Next = Next;
        }
    }

    public class SendLinkedList
    {
        public Node Head { get; set; }
        public Node Tail { get; set; }

        public Node InsertNode(char c, int count)
        {
            Node node = new Node(c, count);

            if(Head == null)
            {
                Head = node;
                Tail = node;
            }
            else
            {
                //Node temp = Head;

                //while (temp.Next != null)
                //{
                //    temp = temp.Next;
                //}
                Node temp1 = Tail;
                Tail = node;
                temp1.Next = node;
            }
            return Head;
        }
    }
    public class StringChar
    {
        public char S { get; set; }
        public int Count { get; set; }

        public StringChar(char S, int Count)
        {
            this.S = S;
            this.Count = Count;
        }
    }
    public class MyString
    {
        public void Compress()
        {
            string s = "bcaabddeafee";
            // Char[] inputChar = s.ToCharArray();
            int len = s.Length;
            int counter = 1;
            StringBuilder bd = new StringBuilder();

            for (int i = 1; i < len; i++)
            {
                char previousChar = s[i - 1];
                if (previousChar == s[i])
                {
                    counter++;
                    continue;
                }
                else
                {
                    bd.Append(counter).Append(previousChar);
                    counter = 1;
                }
            }
            Console.WriteLine(bd);
        }
        public void Decompress()
        {
            string s = "1b1c2ab2d1e1f2e";
            StringBuilder bd = new StringBuilder();
            // char[] inputChar = s.ToCharArray();
            int len = s.Length;
            // int g = (int)Char.GetNumericValue(inputChar[0]);
            for (int i = 0; i < len; i++)
            {
                if (char.IsDigit(s[i]))
                {
                    int count = (int)Char.GetNumericValue(s[i]);
                    for (int j = 0; j < count; j++)
                    {
                        bd.Append(s[i + 1]);
                    }

                    i = i + 1;
                }
                else
                {
                    bd.Append(s[i]);
                }
            }
            Console.WriteLine(bd);
        }
        public Node SendChars()
        {
            List<StringChar> L = new List<StringChar>();
            var lnkd = new SendLinkedList();
            string S = "bcaabddeafee";
            int n = S.Length;
            int count = 1;
            for (int i = 0; i < n; i++)
            {
                int next = i + 1;
                if (next < n && S[i] == S[next])
                {
                    count++;
                    continue;
                }
                if (count > 1)
                {
                    // L.Add(new StringChar(S[i], count));
                    lnkd.InsertNode(S[i], count);
                     count = 1;
                    continue;
                }

                if (S[i] != S[next])
                {
                    //L.Add(new StringChar(S[i], 1));
                    lnkd.InsertNode(S[i], 1);
                }
            }
            return lnkd.Head;
        }
        //remove dupicate chars
        public string RemoveDu(string key)
        {
            {
                // --- Removes duplicate chars using string concats. ---
                // Store encountered letters in this string.
                // string table = "";

                // Store the result in this string.
                string result = "";

                // Loop over each character.
                foreach (char value in key)
                {
                    // See if character is in the table.
                    if (result.IndexOf(value) == -1)
                    {
                        // Append to the table and the result.
                        //  table += value;
                        result += value;
                    }
                }
                return result;
            }
        }
        //pelindrome
        /*
         Palindromes can be read in both directions. 
         How can you determine if a string is a palindrome in the C# language?
         A palindrome has the same letters on both ends of the string. ex: kayak
        */
        public bool IsPalindrome(string word)
        {
            int min = 0;
            int max = word.Length - 1;
            //while (true)
            //{
            //    if (min > max)
            //    {
            //        return true;
            //    }
            //    char a = word[min];
            //    char b = word[max];
            //    if (char.ToLower(a) != char.ToLower(b))
            //    {
            //        return false;
            //    }
            //    min++;
            //    max--;
            //}

            for (int i = word.Length - 1; i >= 0 - 1; i--)
            {
                if (min > i)
                {
                    break;
                }

                if (word[min] != word[i])
                {
                    return false;
                }

                min++;
            }

            return true;
        }

        public int LongestPelidromSubseq(Char[] seq, int i, int j)
        {
            //string s = "GEEKSFORGEEKS";
            //int i = 0;
            //int j = s.Length - 1;

            // Base Case 1: If there is only 1 character  
            if (i == j)
            {
                return 1;
            }

            // Base Case 2: If there are only 2 characters and both are same  
            if (seq[i] == seq[j] && i + 1 == j)
            {
                return 2;
            }

            // If the first and last characters match  
            if (seq[i] == seq[j])
            {
                return LongestPelidromSubseq(seq, i + 1, j - 1) + 2;
            }

            // If the first and last characters do not match  
            return Max(LongestPelidromSubseq(seq, i, j - 1), LongestPelidromSubseq(seq, i + 1, j));

        }
        private int Max(int a, int b)
        {
            return a > b ? a : b;
        }
        public void Reverse()
        {
            string s = "the best is this";
            StringBuilder s1 = new StringBuilder();
            string s3 = "";
            for (int i = s.Length - 1; i >= 0; i--)
            {
                if (s[i] == ' ')
                {
                    s1.Append(' ');
                    s3 += ' ';
                }
                else
                {
                    s1.Append(s[i]);
                    s3 += s[i];
                }
            }
            Console.WriteLine(s1);
            Console.WriteLine(s3);
        }

        public void ReverseInOrder()
        {
            char[] c = { 'a', 'c', '*', 'd', 'a', '?', '!', '7', 'd', '*', 'e' };
            int j = c.Length - 1;
            int i = 0;

            while (i < j)
            {
                char s = c[i];
                char e = c[j];

                if (char.IsLetterOrDigit(c[i]) && char.IsLetterOrDigit(c[j]))
                {
                    char temp = c[i];
                    c[i] = c[j];
                    c[j] = temp;
                    j--;
                    i++;
                    continue;
                }

                if (!char.IsLetterOrDigit(c[i]) && !char.IsLetterOrDigit(c[j]))
                {
                    j--;
                    i++;
                    continue;
                }

                if (!char.IsLetterOrDigit(c[i]))
                {
                    i++;
                    continue;
                }
                if (!char.IsLetterOrDigit(c[j]))
                {
                    j--;
                    continue;
                }

            }

            foreach (var item in c)
            {
                Console.Write(item);
            }
        }

        public bool IsAtEvenIdex(string s, char c)
        {
            //Base case

            if(string.IsNullOrEmpty(s))
                return false;
            for (int i = 0; i < s.Length/2; i = i + 2)
            {
                if (s[i] == c)
                    return true;
            }
            return false;
        }
    }

    public string ReverseEachWord(string s)
    {
        var arr = s.Split();
    }
}
