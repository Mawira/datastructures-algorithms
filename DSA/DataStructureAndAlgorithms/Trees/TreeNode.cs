﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Trees
{
    public class TreeNode
    {
        public int value { get; set; }
        public TreeNode LeftNode { get; set; }
        public TreeNode RightNode { get; set; }
        public TreeNode(int data)
        {
            value = data;
        }
    }
}
