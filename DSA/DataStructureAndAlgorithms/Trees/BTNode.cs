﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Trees
{
   
    public class BTNode
    {
        public int value { get; set; }
        public BTNode LeftNode { get; set; }
        public BTNode RightNode { get; set; }
        //public bool isDeleted { get; set; }
        public BTNode(int Data)
        {
            this.value = Data;
            this.LeftNode = null;
            this.RightNode = null;
           // this.isDeleted = false;
        }
    }
}
