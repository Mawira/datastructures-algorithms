﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Trees.AVLTree
{
    //https://www.youtube.com/watch?v=aZjYr87r1b8
    public class AVLTree
    {
        public AVLNode Root { get; set; }
        AVLOperation op = new AVLOperation();
        public AVLNode Insert(int data)
        {
           
            AVLNode node = new AVLNode(data);
            if(Root == null)
            {
                Root = node;
                return node;
            }
            else
            {
                AVLNode r = op.Insert(Root, node);
              return  op.Insert(Root, node);
            }
        }
    }
}
