﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Trees.AVLTree
{
    public class AVLNode
    {
        public int Key { get; set; }
        public AVLNode RightChild { get; set; }
        public AVLNode LeftChild { get; set; }
        public int Height { get; set; }

        public AVLNode(int Key)
        {
            this.Key = Key;
            Height = 0;
        }
    }
}
