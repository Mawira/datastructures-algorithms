﻿using System;
using System.Collections.Generic;
using System.Text;
//https://www.youtube.com/watch?v=TDEUtGrCBcg
//https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
namespace DSA.DataStructureAndAlgorithms.Trees.AVLTree
{
    public class AVLOperation
    {
        public AVLNode Insert(AVLNode current, AVLNode node)
        {
            if (current == null)
            {
                return node;
            }

            if (node.Key > current.Key)
            {
                current.RightChild = Insert(current.RightChild, node);
            }
            else
            {
                current.LeftChild = Insert(current.LeftChild, node);
            }
            int leftHeight = Height(current.LeftChild);
            int rightHeight = Height(current.RightChild);

            current.Height = MaxHeight(leftHeight, rightHeight) + 1;

            int bf = BalancingFoctor(current);

            //Right right case => rotate left

            if(bf < 0 && node.Key > current.LeftChild.Key)
            {
                return RotateLeft(current);
            }
            //left left case => rotate right
            if(bf > 1 && node.Key < current.LeftChild.Key)
            {
                AVLNode t = RotateRight(current);
                return RotateRight(current);
            }
            //left right case => rotate left then right
            if(bf > 1 && node.Key > current.LeftChild.Key)
            {
                current.LeftChild = RotateLeft(current.LeftChild);
                return RotateRight(current.RightChild);
            }

            //right left case => rotate right then left
            if (bf < 0 && node.Key < current.LeftChild.Key)
            {
                current.RightChild = RotateRight(current.RightChild);
                return RotateLeft(current.LeftChild);
            }

            return current;
        }

        public int Height(AVLNode node)
        {
            if(node == null)
            {
                return 0;
            }
            else
            {
                int h = node.Height;
                return node.Height;
            }   
        }

        public int MaxHeight(int a, int b)
        {
            return a > b ? a : b;
        }
        public int BalancingFoctor(AVLNode node)
        {
            if(node == null)
            {
                return 0;
            }
            else
            {
                int i = Height(node.LeftChild) - Height(node.RightChild);
                return (Height(node.LeftChild) - Height(node.RightChild));
            }
           
        }
        public AVLNode RotateLeft(AVLNode node)
        {
            AVLNode temp1 = node.RightChild;
            AVLNode temp2 = temp1.LeftChild;
            temp1.LeftChild = node;
            node.RightChild = temp2;

            node.Height = Math.Max(Height(node.LeftChild), Height(node.RightChild)) + 1;
            temp1.Height = Math.Max(Height(temp1.LeftChild), Height(temp1.RightChild)) + 1;

            return temp1;
        }

        public AVLNode RotateRight(AVLNode node)
        {
           
                AVLNode temp1 = node.LeftChild;
                AVLNode temp2 = temp1.RightChild;
                temp1.RightChild = node;
                node.LeftChild = temp2;

                node.Height = Math.Max(Height(node.LeftChild), Height(node.RightChild));
                temp1.Height = Math.Max(Height(temp1.LeftChild), Height(temp1.RightChild));

                return temp1;
        }

        public bool BruceForceSearch(int[] arr, int target)
        {
            
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == target)
                {
                    return true;
                }
            }

            return false;

            //O(n)
            //O(1)
        }
        public int BinarySearch(int[] arr, int target)
        {
            int right = arr.Length - 1;
            int left = 0;
            int mid = (right + left) / 2;

           while (left <= right)
            {
                if (arr[mid] == target)
                {
                    return mid;
                }
                else if (target < arr[mid])
                {
                    right = mid - 1;
                }
                else
                {
                    left = mid + 1;
                }
            }
            return -1;//O(1) O(logn)
        }
    }
}
