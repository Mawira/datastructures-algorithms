﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Trees
{
    public class Operation
    {
        public void BTInsert(BTNode Tnode, BTNode node)
        {
            BTNode rightNode = Tnode.RightNode;
            BTNode leftNode = Tnode.LeftNode;

            if (node.value > Tnode.value)
            {
                if (rightNode == null)
                {
                    Tnode.RightNode = node;
                }
                else
                {
                    BTInsert(rightNode, node);
                }
            }
            else
            {
                if (leftNode == null)
                {
                    Tnode.LeftNode = node;
                }
                else
                {
                    BTInsert(leftNode, node);
                }

            }
            return;
        }

        public Node Insert1(Node root, int value)
        {
            if(root == null)
            {
                root = new Node();
                root.Data = value;
            }
            else
            {
                if(value < root.Data)
                {
                    root.Left = Insert1(root.Left, value);
                }
                else
                {
                    root.Right = Insert1(root.Right, value);

                }
            }
            return root;
        }
        public TreeNode InsertV2(TreeNode root, TreeNode node)
        {

            if (root == null)
                return new TreeNode(node.value);


            TreeNode leftNode = root;
            TreeNode rightNode = root;

            if (leftNode.value < node.value)
            {
                if (leftNode.LeftNode == null)
                {
                    leftNode.LeftNode = node;
                }
                else
                {
                    InsertV2(leftNode, node);

                }

            }
            if (rightNode.value > node.value)
            {
                if (rightNode.RightNode == null)
                {
                    rightNode.RightNode = node;
                }
                else
                {
                    InsertV2(rightNode, node);

                }

            }
            return root;
        }
        public void Insert(BTNode root, BTNode node)
        {

            BTNode tempNode = root;
            bool isAdded = false;

            while (!isAdded)
            {
                if (node.value < tempNode.value)
                {
                    if (tempNode.LeftNode == null)
                    {
                        tempNode.LeftNode = node;
                        isAdded = true;
                    }
                    else
                    {
                        tempNode = tempNode.LeftNode;
                    }
                }
                else if (node.value > tempNode.value)
                {
                    if (tempNode.RightNode == null)
                    {
                        tempNode.RightNode = node;
                        isAdded = true;
                    }
                    else
                    {
                        tempNode = tempNode.RightNode;
                    }
                }
            }



        }
        public BTNode Find(BTNode root, BTNode node)
        {
            BTNode currentNode = root;

            while (currentNode != null)
            {
                if (currentNode.value == node.value)
                {
                    return currentNode;
                }
                else if (node.value > currentNode.value)
                {
                    currentNode = currentNode.RightNode;
                }
                else
                {
                    currentNode = currentNode.LeftNode;
                }
            }
            return null;
        }
        public BTNode FindRecursive(BTNode btNode, BTNode node)
        {
            BTNode leftNode = btNode.LeftNode;
            BTNode rightNode = btNode.RightNode;
            if (node.value == btNode.value)
            {
                return btNode; //termanation
            }
            else if (node.value < btNode.value && leftNode != null)
            {
                return FindRecursive(leftNode, node);
            }
            else if (rightNode != null)
            {
                return FindRecursive(rightNode, node);
            }
            else
            {
                return null;
            }
        }
        public void PostOrderTraversal(BTNode node)
        {
            BTNode rightNode = node.RightNode;
            BTNode leftNode = node.LeftNode;
            if (leftNode != null)
                PostOrderTraversal(leftNode);  //75,50,40,90,86,93,32,44,7,100,101,105,110
            if (rightNode != null)
                PostOrderTraversal(rightNode);

            Console.Write(node.value + ",");
        }
        public void InOrderTraversal(BTNode node)
        {
            BTNode leftNode = node.LeftNode;
            BTNode rightNode = node.RightNode;

            if (leftNode != null)
                InOrderTraversal(leftNode);

            Console.Write(node.value + " ");

            if (rightNode != null)
                InOrderTraversal(rightNode);
        }
        public void PreOrderTraversal(BTNode node)
        {
            BTNode leftNode = node.LeftNode;
            BTNode rightNode = node.RightNode;

            Console.Write(node.value + " ");

            if (leftNode != null)
                PreOrderTraversal(leftNode);

            if (rightNode != null)
                PreOrderTraversal(rightNode);
        }

        public void RemoveNode(BTNode root, BTNode node)
        {
            BTNode parent = root;
            BTNode current = root;
            //bool isLeftChild = false;

            if (root.RightNode == null && root.LeftNode == null)// has ony one node
            {
                root = null;
                return;
            }

            if (current == null)
            {
                return;
            }

            while (current != null && current.value != node.value)
            {
                parent = current;

                if (node.value < current.value)
                {
                    current = current.LeftNode;
                    //isLeftChild = true;
                }
                else
                {
                    current = current.RightNode;
                    //  isLeftChild = false;
                }
            }

            if (current == null)
            {
                return;
            }

            if (current.RightNode == null && current.LeftNode == null)// no children
            {
                if (parent.RightNode.value == current.value)
                {
                    parent.RightNode = null;
                }
                else
                {
                    parent.LeftNode = null;
                }

                //if (isLeftChild)
                //{
                //    parent.LeftNode = null;
                //}
                //else
                //{
                //    parent.RightNode = null;
                //}
            }
            else if (current.LeftNode == null)//only has left child
            {
                parent.RightNode = current.RightNode;
            }
            else if (current.RightNode == null) //has only left child
            {
                parent.LeftNode = current.LeftNode;
            }
            else // has both left and right child
            {
                BTNode successor = GetSuccessor(current);
                if (current == root)
                {
                    root = GetSuccessor(current);
                }
                else if (parent.RightNode == current.RightNode)
                {
                    parent.RightNode = successor;
                }
                else
                {
                    parent.LeftNode = successor;
                }
            }
        }
        private BTNode GetSuccessor(BTNode node)
        {
            BTNode parent = node;
            BTNode current = node.RightNode;
            BTNode successor = node;
            if (current.LeftNode != null)
            {
                while (current != null)
                {
                    parent = successor;
                    successor = current;
                    current = current.LeftNode;
                }
            }
            else
            {
                successor = current;
            }

            if (node.RightNode != successor)
            {
                successor.RightNode = node.RightNode;
                successor.LeftNode = node.LeftNode;
                parent.LeftNode = null;
                node = null;
            }
            else
            {
                successor.LeftNode = node.LeftNode;
                node = null;
            }
            return successor;
        }

        public void SoftDelete(BTNode root, BTNode node)
        {
            //BTNode btNode = Find(root,node);
            //btNode.isDeleted = true;
        }
        public BTNode GetSmallest(BTNode root)
        {
            BTNode current = root;
            BTNode node = root;

            while (current != null)
            {
                node = current;
                current = current.LeftNode;
            }
            return node;
        }
        public BTNode GetBiggest(BTNode root)
        {
            BTNode current = root;
            BTNode node = root;

            while (current != null)
            {
                node = current;
                current = current.RightNode;
            }
            return node;
        }
        public BTNode GetBiggestInLeftSubT(BTNode root)
        {
            BTNode current = root.LeftNode;
            BTNode node = root;

            while (current != null)
            {
                node = current;
                current = current.RightNode;
                //go left for smallest = smallest for the tree
            }
            return node;
        }
        public BTNode GetSmallestnRightSubT(BTNode root)
        {
            BTNode current = root.LeftNode;
            BTNode node = root;

            while (current != null)
            {
                node = current;
                current = current.LeftNode;
                //go right for biggest = biggst for the tree
            }
            return node;
        }
        public int NumberOfLeafNode(BTNode node)//L = T + 1 T=> no of internal nodes L => leaf nodes or L => 2h-1
        {
            BTNode leftNode = node.LeftNode;
            BTNode rightNode = node.RightNode;
            if (node == null)
                return 0;

            if (node.RightNode == null && node.LeftNode == null)
            {
                return 1;
            }
            else
            {
                return NumberOfLeafNode(leftNode) + NumberOfLeafNode(rightNode);
            }


            /*
                           4
                         /   \
                        8     10
                       /     /   \
                      7     5     1
                     /
                    3
            */
        }

        public int HeightOfTree(BTNode node)
        {
            if (node.RightNode == null && node.LeftNode == null)
            {
                return 0;
            }

            BTNode rightNode = node.RightNode;
            BTNode leftNode = node.LeftNode;
            int right = 0;
            int left = 0;

            if (leftNode != null)
            {
                left = HeightOfTree(leftNode);
            }
            if (rightNode != null)
            {
                right = HeightOfTree(rightNode);
            }

            //if(left > right)
            //{
            //    return (left + 1);
            //}
            //else
            //{
            //    return right + 1;
            //}
            return left > right ? left + 1 : right + 1;
        }
    }
}
