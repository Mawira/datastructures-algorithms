﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Trees
{
    public class BTree
    {
        Operation op = new Operation();
        public BTNode Root { get; set; }
        public TreeNode RootV2 { get; set; }

        //O(logn) NB - if the tree is balance => O(log2n)
        public void Insert(int value)
        {
            TreeNode btNode = new TreeNode(value);

                RootV2 = op.InsertV2(RootV2, btNode);

            //if (Root != null)
            //{
            //  op.BTInsert(Root, btNode);
            //}

            //if(Root == null)
            //{
            //    Root = new BTNode(value);
            //}
        }
        //O(logn) NB - if the tree is balance => O(log2n)
        public BTNode Find(int value)
        {
            if (Root != null)
            {
                BTNode btNode = new BTNode(value);
                return op.Find(Root, btNode);
            }
            else
            {
                return null;
            }
        }
        public BTNode FindRecursive(int value)
        {
            if (Root != null)
            {
                BTNode bt = new BTNode(value);
                return op.FindRecursive(Root, bt);
            }
            return null;
        }
        public void RemoveNode(int value)
        {
            BTNode node = new BTNode(value);
            if (Root != null)
            {
                op.RemoveNode(Root, node);
            }
        }
        public void SoftDelete(int value)
        {
            if (Root != null)
            {
                BTNode data = new BTNode(value);
                op.SoftDelete(Root, data);
            }
        }
        //InOrderTraversal (left -> root -> right
        public void InOrderTraversal()
        {
            if (Root != null)
            {
                op.InOrderTraversal(Root);
            }
        }
        //PreOrderTraversal root -> left -> right
        public void PreOrderTraversal()
        {
            if (Root != null)
            {
                op.PreOrderTraversal(Root);
            }
        }
        //left -> right -> root
        public void PostOrderTraversal()
        {
            BTNode bt = new BTNode(Root.value);
            if (Root != null)
            {
                op.PostOrderTraversal(Root);
            }
        }
        public int? GetSmallest()
        {
            if (Root.LeftNode == null)
            {
                return Root.value;
            }
            if (Root != null)
            {
                //var node = op.GetSmallest(Root);
                return op.GetSmallest(Root).value;
            }
            return null;
        }
        public int? GetSmallestInLeftSubT()
        {
            //var node = op.GetBiggestInLeftSubT(Root);
            if (Root.LeftNode == null)
            {
                return Root.value;
            }
            if (Root != null)
            {
                return op.GetBiggestInLeftSubT(Root).value;
            }
            return null;
        }
        public int? GetBiggest()
        {
            if (Root.RightNode == null)
            {
                return Root.value;
            }
            if (Root != null)
            {
                // var node = op.GetBiggest(Root);
                return op.GetBiggest(Root).value;
            }
            return null;
        }

        public void NumberOfLeafNode()
        {
            if (Root != null)
            {
                Console.WriteLine(op.NumberOfLeafNode(Root));
            }
        }

        public void HeightOfBT()
        {
            if (Root != null)
            {
                Console.WriteLine(op.HeightOfTree(Root));
            }
        }
    }
}
