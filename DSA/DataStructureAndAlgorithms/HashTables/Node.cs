﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.HashTable
{
   public class Node<T>
    {
        public T Item { get; set; }
        public string Key { get; set; }
        public Node<T> Next { get; set; }
    }
}
