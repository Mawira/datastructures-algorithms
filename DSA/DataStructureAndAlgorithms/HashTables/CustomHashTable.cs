﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.HashTable
{
    //this are reference link
    //https://www.youtube.com/watch?v=LTAAn97QBH8&t=209s
    // https://www.youtube.com/watch?v=iKDhgVoXVTk
    //https://www.includehelp.com/data-structure-tutorial/collisions-in-hashing-and-collision-resolution-techniques.aspx
    //https://www.youtube.com/watch?v=E7Voso411Vs
    //https://simpledevcode.wordpress.com/2015/07/07/hash-tables-tutorial-c-c-java/
    public class CustomHashTable
    {
        //double hashing
        public string[] HashArray { get; set; }
        public int ArraySize { get; set; }

        public CustomHashTable(int size)
        {
            ArraySize = size;
            if (!isPrime(ArraySize))
            {
                ArraySize = NextPrime(ArraySize);
            }
            HashArray = new string[ArraySize];
        }

        public bool isPrime(int size)
        {
            for (int i = 2; i <= size / 2; i++)
            {
                if (size % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public int NextPrime(int size)
        {

            for (int i = size; true; i++)
            {
                if (isPrime(i))
                {
                    return i;
                }

            }

        }
        public void Insert(string word)
        {

            int index = Func1(word);

            if (HashArray[index] == null)
            {
                HashArray[index] = word;
            }
            else
            {
                while (HashArray[index] != null)
                {
                    index = index + Func2(word);
                }
                if(HashArray[index] == null)
                {
                    HashArray[index] = word;
                }
                else
                {
                    index = Func1(word);

                    for (int i = 0; i < index; i++)
                    {
                        if (HashArray[i] == null)
                        {
                            HashArray[i] = word;
                            return;
                        }
                            
                    }
                    return;
                }
            }


        }
        public void PrintHT()
        {
            foreach (var entry in HashArray)
            {
                Console.WriteLine(entry);
            }
        }
        // return preffered index location
        public int Func1(string word)
        {
            int hashValue = word.GetHashCode();

            hashValue = Math.Abs(hashValue);

            hashValue = hashValue % ArraySize;
            return hashValue;
        }
        // return step size greater than zero
        public int Func2(string word)
        {
            int hashValue = word.GetHashCode();

            hashValue = Math.Abs(hashValue);

            hashValue = hashValue % ArraySize;
            //always use a prime number that is less than the arraysize>>> here am using three
            return 3 + hashValue % 3;
        }
        //best case complexity O(1) for delete,find insert, and O(n) for worst case
        public string Find(string key)
        {
            int index = Func1(key);
            while (HashArray[index] != null)
            {
                if (HashArray[index] == key)
                {
                    return HashArray[index];
                }
                index = index + Func2(key);
            }
            return HashArray[index];

        }

        //linear probing
        public void LinearBrobinInsert(string word)
        {
            int hashCode = word.GetHashCode();
            hashCode = Math.Abs(hashCode);
            int index = hashCode % ArraySize;

            while(HashArray[index] != null)
            {
                index++;
            }
            HashArray[index] = word;
        }
    }
}
