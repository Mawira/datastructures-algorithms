﻿using System;
using System.Collections.Generic;
using System.Text;
//https://www.youtube.com/watch?v=iKDhgVoXVTk
namespace DSA.DataStructureAndAlgorithms.HashTable
{
    public class HTSeparateChaining<T>
    {
        private Node<T>[] buckets { get; set; }
        public int Size { get; set; }

        public HTSeparateChaining(int Size)
        {
            this.Size = Size;
            buckets = new Node<T>[Size];
        }

        public void AddItem(string key, T value)
        {
            isValidKey(key);

            Node<T> valueNode = new Node<T>
            {
                Item = value,
                Key = key,
                Next = null
            };
            int index = GetPosition(key, Size);
            Node<T> listNode = buckets[index];

            if (listNode == null)
            {
                buckets[index] = valueNode;
            }

            else
            {
                while (listNode.Next != null)
                {
                    listNode = listNode.Next;
                }
                listNode.Next = valueNode;
            }

        }

        public T Get(string key)
        {
            var node = GetNodeByKey(key);
            if (node == null)
            {
                throw new ArgumentOutOfRangeException($"{nameof(key)} the key {key} is not found");
            }
            return node.Item;
        }

        public  Node<T>  GetNodeByKey(string key)
        {
            isValidKey(key);
            int index = GetPosition(key, Size);

            Node<T> previous = null;
            Node<T> listNode = buckets[index];

            while (listNode != null)
            {
                if (listNode.Key == key)
                {
                    return  listNode;
                }
                else
                {
                    previous = listNode;
                    listNode = listNode.Next;
                }
            }
            return  null;
        }
        public void Remove(string key)
        {
            isValidKey(key);
            int index = GetPosition(key, Size);

            Node<T> listNode = buckets[index];
            if(listNode.Next != null)
            {
                Node<T> previousNode = null;
                Node<T> nextNode = null;
                while (listNode != null)
                {
                    if (listNode.Key == key)
                    {
                        break;
                    }
                    previousNode = listNode;
                    listNode = listNode.Next;
                    nextNode = listNode.Next;
                }
                previousNode.Next = nextNode;
            }
            else
            {
                buckets[index] = null;
            }
        }

        public bool Contains(string key)
        {
            isValidKey(key);

            //int index = GetPosition(key, Size);
            //Node<T> listNode = buckets[index];
            //while (listNode != null)
            //{
            //    if(listNode.Key == key)
            //    {
            //        return true;
            //    }

            //    listNode = listNode.Next;
            //}

           var node = GetNodeByKey(key);

            return node != null;
        }
        public int GetPosition(string key, int size)
        {
            var count = 0;
            for (int i = 0; i < key.Length; i++)
            {
                count++;
            }
            //int index = key.GetHashCode();
            return Math.Abs(count % size);
        }

        protected void isValidKey(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentNullException(nameof(key));
            }
        }
    }
}
