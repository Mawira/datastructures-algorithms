﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Stacks
{
    public class BasicStack<T>
    {
        public Node<T> Head { get; set; }
        public Node<T> Tail { get; set; }
        private T[] Items { get; set; }
        private int size { get; }
        public int Index { get; set; }// the next first empty index;

        public BasicStack(int Size)
        {
            Index = 0;
            this.size = Size;
            Items = new T[Size];
        }

        public void Push(T item)
        {
            if (Index == size)
            {
                T[] newStack = new T[2 * size];
                Array.Copy(Items, 0, newStack, 0, Index);
                Items = newStack;               
            }
            Items[Index] = item;

            Index++;
        }
        public T Pop()
        {
            if (Index == 0)
                throw new InvalidOperationException("No items in the stack");

            var value = Items[--Index];
            Index--;
            return value;
        }

        public T Peek()//return the top element without removing it
        {
            if (Index == 0)
                throw new InvalidOperationException("No items in the stack!");

            return Items[--Index];
        }
        public void Print()
        {
            for (int i = 0; i <= Index; i++)
            {
                Console.WriteLine(Items[i]);
            }
        }

        //implementation using linked list
        public void Insert(T value)
        {
            var node = new Node<T>(value);
            if(Head == null)
            {
                Head = node;
            }
            else
            {
                node.Next = Head;
                Head = node;
            }
        }

        public void RemovePop()
        {
            if (Head == null)
                return;
            else
            {
                Node<T> temp = Head.Next;
                Head.Next = null;
                Head = temp;
            }
        }
        public void NextGreaterElement(int[] arr)
        {
            Stack<int> stack = new Stack<int>();

            stack.Push(arr[0]);

            for (int i = 1; i < arr.Length; i++)
            {
                int next = arr[i];

                if(stack.Count > 0)
                {
                    int popped = stack.Pop();
                    while(popped < next)
                    {
                        Console.Write(popped + "--->" + next);
                        if (stack.Count == 0)
                            break;
                        popped = stack.Pop();   
                    }
                    if(popped > next)
                    {
                        stack.Push(next);
                    }
                }

                while(stack.Count > 0)
                {
                    Console.Write(stack.Pop() + "--->" + -1);
                }
            }
        }

        public bool HasMatchingParenthesis(string S)//S = "{}}((}}{}}})[[]]"
        {
            Stack<char> stack = new Stack<char>();

            foreach (char c in S)
            {
                if (c == '(' || c == '{' || c == '[')
                {
                    stack.Push(c);
                }
                else if (c == ')' || c == '}' || c == ']')
                {
                    if (stack.Count == 0)
                    {
                        return false; // No opening parenthesis to match with
                    }

                    char top = stack.Peek();
                    if ((c == ')' && top == '(') || (c == '}' && top == '{') || (c == ']' && top == '['))
                    {
                        stack.Pop();
                    }
                    else
                    {
                        return false; // Mismatched parenthesis
                    }
                }
            }

            return stack.Count == 0;//balanced
        }
        //If one symbol is involved
        public bool HasMatchingParenthesis2(string S)
        {
            int matchingsymbolTracker = 0;

            foreach (char c in S)
            {
                if(c == '(')
                {
                    matchingsymbolTracker++;
                }

                if(c == ')')
                {
                    if(matchingsymbolTracker > 0)
                    {
                        matchingsymbolTracker--;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return matchingsymbolTracker == 0;
        }
    }
}
