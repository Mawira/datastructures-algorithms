﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Arrays
{
    public class RecursionMtd
    {
        //factorial given n

        //n! = n.(n-1)(n-2)....2*1

        //n*(n-1)!recursion
        int count = 0;

        public int fact(int n)
        {

            if (n == 0)
            {
                return 1;
            }
            else
            {
                return n * fact(n - 1);
            }
        }

        public void Factorial(int n)
        {
            int factorial = n;
            for (int i = 1; i < n; i++)
            {
                factorial *= n - i;
            }
            Console.WriteLine(factorial);
        }
        //fibonacci sequence

        // 1,1,2,3,5,8,13,21 => n = (n-1) + (n-2)

        public int Fib(int n)
        {//recursion mtd

            //if (n == 0)
            //{
            //    count++;
            //    return 0;
            //}
            //if (n == 1)
            //{
            //    count++;
            //    return 1;
            //}
            //count++;
            //return Fib(n - 1) + Fib(n - 2);

            int[] A = new int[n];

            A[0] = 1;
            A[1] = 1;

            for (int i = 2; i < n; i++)
            {
                A[i] = A[i - 1] + A[i - 2];
            }
            return A[n - 1];
        }

        public void PrintCount(int count)
        {
            Console.Write(count);
        }
        //series where current number = the sum of 2 previous consecutive numbers
        public void FibonacciSeries(int len)
        {
            int a = 0, b = 1, c = 0;
            Console.WriteLine(b);
            for (int i = 0; i < len; i++)
            {
                c = a + b;
                Console.WriteLine(c);
                a = b;
                b = c;
            }
            Console.WriteLine("last number of the series {0}", c);
        }

        public bool isPrime(int n)
        {
            for (int i = 2; i <= n/2; i++)
            {
                if(n%i == 0)
                {
                    return false;
                }
                
            }
            return true;
        }
    }
}
