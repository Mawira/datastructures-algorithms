﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Arrays
{
    public class TwoDimArray
    {
        public void CheckIfSumExist()
        {
            //int[] arr = new int[] { 4, 6, 10, 15, 16 };
            //int limit = 21;
            //Hashtable hash = new Hashtable();

            //for (int i = 0; i < arr.Length; i++)
            //{
            //    int w = arr[i];

            //    if (hash.ContainsKey(limit - w))
            //    {
            //        var a = new int[] { i, (int)hash[(limit - w)] };
            //        return a;
            //    }
            //    else
            //    {
            //        hash.Add(w, i);
            //    }
            //}
            //return new int[] { };
            int[] arr = new int[] { 4, 6, 10, 15, 16 };
            int[] result = new int[] { };
            int limit = 21;
            // your code goes here

            //for (int i = 0; i < arr.Length; i++)
            //{

            //    for (int j = i + 1; j < arr.Length; j++)
            //    {
            //        if (arr[i] + arr[j] == limit)
            //        {
            //            result[0] = j;
            //            result[1] = i;
            //        }
            //    }
            //}

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        public void PopulateTwoDimArray()
        {
            int[,] arr = new int[3, 3];

            Random r = new Random();

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = 0; //r.Next(1, 200);
                }
            }

            foreach (var item in arr)
            {
                //  Console.WriteLine(item);
            }

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.WriteLine(arr[i, j]);
                }
            }

        }

        public void MissingInteger()
        {
            int[] A = new int[] { 1, 2, 3 };
            //1,1,2,3,4,6
            Array.Sort(A);
            int n = 0;
            for (int i = 1; i < A.Length; i++)
            {
                int x = A[n];
                int y = A[i];
                if (x == y - 1)
                {
                    n++;
                    continue;
                }
                if (x == y)
                {
                    n++;
                    continue;
                }
                break;
            }

            Console.WriteLine(A[n] + 1);
        }

        public void BinomialExpansion()
        {
            int row = 3;

            //if (row == 1)
            //    return new List<int> { 1 };

            //if (row == 2)
            //    return new List<int> { 1, 1 };

            List<int> list = new List<int> { 1, 1 };

            for (int i = 3; i <= row; i++)
            {
                List<int> current = new List<int>();
                current.Clear();
                current.Add(1);

                for (int j = 1; j < list.Count; j++)
                {
                    int previousRowValue1 = list[j - 1];
                    int previousRowValue2 = list[j];

                    current.Add(previousRowValue1 + previousRowValue2);
                }
                current.Add(1);
                list = current;
            }
            Console.WriteLine("this is it");
            foreach (var i in list)
            {
                Console.Write(i + " ");
            }
        }
    }
}
