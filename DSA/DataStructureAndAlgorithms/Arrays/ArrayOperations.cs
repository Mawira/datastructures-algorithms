﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Arrays
{
    public class ArrayOperations
    {
        //https://www.youtube.com/watch?v=0xsGZSGNC4M binary search
        //https://www.youtube.com/watch?v=7U7cXEROrGY recursive mtd
        //Get GCD https://www.youtube.com/watch?v=QohAyJ4MPBs
        //https://www.csharpstar.com/csharp-algorithms/
        public void ReverseArrayInMemory()
        {

            var array = new int[] { 6, 4, 3, 3, 5, 7, 2, 1 };
            int i = 0; // start Index
            int j = array.Length - 1;//last index

            while (i <= j)
            {
                array[i] += array[j];
                array[j] = array[i] - array[j];
                array[i] = array[i] - array[j];
                //int temp = array[i];
                //array[i] = array[j];
                //array[j] = temp;
                j--;
                i++;

            }
            for (int k = 0; k <= array.Length - 1; k++)
            {
                Console.WriteLine(array[k]);
            }
        }
        //insertion sort
        //public void SortArray()
        //{
        //    var array = new int[] { 6, 4, 4, 1, 5, 7 ,1,3};

        //    for (int i = 1; i < array.Length; i++)
        //    {
        //        int j = i - 1;
        //        int n = i;
        //        while (array[n] != array[array.Length - 1])
        //        {
        //            if (array[j] > array[n])
        //            {
        //                int a = array[j];
        //                array[j] = array[n];
        //                array[n] = a;
        //            }
        //            else if (array[j] == array[n])
        //            {
        //                int s = array[j + 1];
        //                array[j + 1] = array[n];
        //                array[n] = s;

        //            }
        //            else
        //            {
        //                n++;
        //            }
        //        }

        //    }

        //    for (int i = 0; i < array.Length; i++)
        //    {
        //        Console.WriteLine(array[i]);
        //    }

        //    //complexity O(n2) for the worst case -- O(n) best case
        //}
        public int[] MergeSort(int[] A, int lb, int ub)
        {
            if (lb < ub)
            {
                int mid = (ub + lb) / 2;
                int[] left = MergeSort(A, lb, mid);
                int[] right = MergeSort(A, mid + 1, ub);


                int[] result = new int[left.Length + right.Length];

                int idxL = 0;
                int idxR = 0;
                int i = 0;

                for (; idxL < left.Length && idxR < right.Length; i++)
                {
                    if (left[idxL] < right[idxR])
                    {
                        result[i] = left[idxL];
                        idxL++;
                    }
                    else
                    {
                        result[i] = left[idxR];
                        idxR++;
                    }
                }

                while (idxL < left.Length)
                {
                    result[i] = left[idxL];
                    idxL++;
                }

                while (idxR < right.Length)
                {
                    result[i] = right[idxR];
                    idxR++;
                }

                return result;
            }
            return A;
        }

        public int[] Merge(int[] A, int lb, int ub, int mid)
        {

            int i = lb;
            int j = mid + 1;
            int k = lb;
            int[] B = new int[ub];

            while (i <= mid && j <= ub)
            {
                if (A[i] <= A[j])
                {
                    B[k] = A[i];
                    i++;
                }
                else
                {
                    B[k] = A[j];
                    j++;
                }
                k++;
            }

            if (i > mid)
            {
                while (j < ub)
                {
                    A[k] = A[j];
                    k++;
                    j++;
                }
            }
            else
            {
                while (i < mid)
                {
                    A[k] = A[i];
                    i++;
                    k++;
                }
            }

            return A;
        }
        public int[] QuickSort(int[] a, int lb, int ub)
        {
            /// var A = new int[] { 6, 4, 4, 1, 5, 7, 1, 3 };
            if (lb < ub)
            {
                int c = Partition(a, lb, ub);
                QuickSort(a, lb, c - 1);
                QuickSort(a, c + 1, ub);
            }
            return a;
        }

        public void Print()
        {
            int[] a = { 4, 4, 3, 3, 1, 0 };
            var arr = QuickSort(a, 0, a.Length - 1);
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine(a[i]);
            }
        }
        public int Partition(int[] a, int lb, int ub)
        {
            int start = lb;
            int end = ub - 1;
            int pivot = a[ub];

            for (int i = lb; i < ub; i++)
            {
                if (a[start] < pivot)
                {
                    start++;
                }
                else
                {
                    int temp = a[start];
                    a[start] = a[end];
                    a[end] = temp;
                    end--;

                    // Swap(a, start, end);

                }
            }
            int temp1 = a[start];
            a[start] = pivot;
            a[ub] = temp1;
            // Swap(a, start, ub);

            return start;
        }
        public void Swap(int[] a, int start, int end)
        {
            int temp = a[start];
            a[start] = a[end];
            a[end] = temp;
        }
        //find the majority element in array
        public int GetMajority(params int[] x)
        {
            x = new int[] { 1, 2, 3, 4, 5, 2, 2, 2, 2 };
            Dictionary<int, int> d = new Dictionary<int, int>();
            int majority = x.Length / 2;

            //Stores the number of occcurences of each item in the passed array in a dictionary
            foreach (int i in x)
                if (d.ContainsKey(i))
                {
                    d[i]++;
                    //Checks if element just added is the majority element
                    if (d[i] > majority)
                        return i;
                }
                else
                    d.Add(i, 1);
            //No majority element
            //throw new Exception("No majority element in array");
            return 0;

            //array = new int[] { 1, 2, 3, 4, 5, 2, 2, 2, 2 };
            //int m = x.Length / 2;

            //if (x.Any(i => x.Count(j => j == i) > m))
            //{
            //    return x.FirstOrDefault(i => x.Count(j => j == i) > x.Length / 2);
            //}
            //return 0;
            //  Console.WriteLine("No majority element in array");
        }
        //reverse number and check if its a pelindrom => reversed = number

        public void ReverseNumber(int num)
        {

            int reminder, temp, reverse = 0;
            temp = num;
            while (num > 0)
            {
                reminder = num % 10;
                reverse = reverse * 10 + reminder;
                num /= 10;
            }

            Console.WriteLine($"number = {temp}");
            Console.WriteLine($"reverse ={reverse}");

            if (temp == reverse)
            {
                Console.WriteLine("number is pelindrom");
            }
            else
            {
                Console.WriteLine("not pelindrom");
            }
        }
        // x is the value to search for, you should not correct more than three line of code
        private int Bs(int[] array, int x)
        {
            int l = 0;
            int r = array.Length - 1;
            while (l < r)
            {
                int m = (l + r) / 2;
                if (x < array[m])
                {
                    r = m - 1;
                }
                else
                {
                    l = m + 1;
                }
            }
            if (x == array[l])
            {
                return array[l];
            }
            return -1;
        }
        public static object BinarySearchDisplay(int[] arr, int key)
        {
            int minNum = 0;
            int maxNum = arr.Length - 1;

            while (minNum <= maxNum)
            {
                int mid = (minNum + maxNum) / 2;
                if (key == arr[mid])
                {
                    return mid;
                }
                else if (key < arr[mid])
                {
                    maxNum = mid - 1;
                }
                else
                {
                    minNum = mid + 1;
                }
            }
            return "None";
        }

        public int GetTallest()
        {
            Hashtable hash = new Hashtable();

            int[] A = { 1, 3, 1, 3 };
            int tall = A[0];
            hash.Add(tall, 1);
            for (int i = 1; i < A.Length; i++)
            {
                if (tall <= A[i])
                {
                    tall = A[i];

                    if (!hash.Contains(tall))
                    {
                        hash.Add(tall, 1);
                    }
                    else
                    {
                        int count = (int)hash[tall];
                        hash[tall] = count + 1;
                    }
                }
            }

            return (int)hash[tall];
        }
        //time complexity O(N) where N is the size of the array
        //best case O(n) where n is the size of potential sequence if it starts ia the begining of the array
        public bool isValidSubSequence()
        {
            int[] arr = { 5, 1, 22, 25, 6, -1, 8, 10 };
            int[] seq = { 5, 1, 22, 25, 6, -1, 8, 10, 12 };

            int seqIndex = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (seqIndex == seq.Length)
                {
                    break;
                }
                if (arr[i] == seq[seqIndex])
                {
                    seqIndex++;
                }
            }
            return seqIndex == seq.Length;
        }

        public void EqualizeArray()
        {
            int[] array = new int[] { 1, 1, 2, 2, 3, 3, 3 };
            int len = array.Length;
            Hashtable table = new Hashtable();
            int maxFreq = 1;

            for (int i = 0; i < len; i++)
            {
                if (!table.ContainsKey(array[i]))
                {
                    table.Add(array[i], 1);
                }
                else
                {
                    int freq = (int)table[array[i]] + 1;
                    table[array[i]] = freq;

                    maxFreq = maxFreq < freq ? freq : maxFreq;
                }
            }

            int minDeletions = len - maxFreq;
        }

        public bool SubSetSum()
        {
            int sum = 30;
            int[] arr = new int[] { 3, 34, 4, 12, 5, 2 };
            int len = arr.Length;

            for (int i = 0; i < len; i++)
            {
                int doubleSum = arr[i] + arr[i + 1];
                for (int j = 1; j < len; j++)
                {
                    if (arr[i] + arr[j] == sum)
                        return true;
                }
            }

            return false;
        }
        //consecutive
        public bool TripleSum()
        {
            int[] arr = { 6, 2, 3, 4 };
            int len = arr.Length;
            int sum = 9;

            for (int i = 0; i < len - 2; i++)
            {
                int tripleSum = arr[i] + arr[i + 1] + arr[i + 2];

                if (tripleSum == sum)
                {
                    return true;
                }
            }
            return false;
        }
        //https://www.geeksforgeeks.org/minimize-count-of-divisions-by-d-to-obtain-at-least-k-equal-array-elements/

        public int EqualizeByDivisionMin()
        {
            int[] arr = new int[] { 60, 30, 25, 33 };
            int len = arr.Length;
            int[] tempArr = new int[len];

            int d = 2;
            int count = 1;
            int op = 0;
            int threshold = 3;
            for (int i = 0; i < len; i++)
            {
                int value = arr[i] / d;
                for (int j = 0; j < len; j++)
                {
                    if (value == arr[j])
                    {
                        tempArr[i] = arr[i];

                        op++;
                        count++;
                    }
                    else
                    {
                        int v = arr[j] / d;
                        tempArr[j] = v;
                        if (value == v)
                        {
                            op++;
                            count++;
                        }
                    }
                    if (count == threshold)
                    {
                        return op;
                    }

                }
                arr = tempArr;
                count = 1;
                op = 0;
            }

            return 0;
        }

        public void SwapToEqualMedian()
        {
            int[] arr1 = { 1, 2, 3, 3, 5, 6, 7 };
            int[] arr2 = { 4, 6, 8, 8, 9, 9, 9 };
            int counter = 0;
            int j = arr1.Length;
            int medianIndex = j / 2 + 1;

            int[] arr3 = new int[j * 2];
            int len1 = j;
            int len2 = j;

            for (int i = 0; i < j * 2; i++)
            {

            }
        }

        public int GetMinMissingNumGreaterThanZero(int[] A)
        {
            Array.Sort(A);
            int j = 0;
            while (true)
            {
                if (j == A.Length - 1)
                {
                    break;
                }
                if (A[j] == A[j + 1] || A[j] + 1 == A[j + 1])
                {
                    j++;
                    continue;
                }
                else
                {
                    if (A[j] > 0)
                    {
                        return A[j] + 1;
                    }
                    j++;
                    continue;
                }
            }
            int l = A.Length;
            int no = A[l - 1];
            while (true)
            {
                no++;

                if (no <= 0)
                {
                    continue;
                }
                break;
            }
            return no;
        }
        //Rotate an array once

        public static int[] RotateArray(int[] arr)
        {
            if (arr.Length <= 1)
                return arr;

            int temp = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = arr[i + 1];
            }

            arr[arr.Length - 1] = temp;
            return arr;
        }
        //Rotate an array k times
        public static int[] RotateArray(int[] arr, int k)
        {
            if (arr.Length <= 1)
                return arr;

            for (int j = 0; j < k; j++)
            {
                int temp = arr[0];
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = arr[i + 1];
                }

                arr[arr.Length - 1] = temp;
            }

            return arr;
        }
        public static int FindMaxSubarraySum(int[] nums)
        {
            int maxSum = nums[0];
            int currentSum = nums[0];

            for (int i = 1; i < nums.Length; i++)
            {
                // Choose between extending the subarray or starting a new subarray
                currentSum = Math.Max(nums[i], currentSum + nums[i]);

                // Update the maximum sum if the current sum is greater
                maxSum = Math.Max(maxSum, currentSum);
            }

            return maxSum;
        }
        //return the array with max sum
        public static int[] FindMaxSubarray(int[] nums)
        {
            int maxSum = nums[0];
            int currentSum = nums[0];
            int start = 0;
            int end = 0;
            int currentStart = 0;

            for (int i = 1; i < nums.Length; i++)
            {
                // Choose between extending the subarray or starting a new subarray
                if (nums[i] > currentSum + nums[i])
                {
                    currentSum = nums[i];
                    currentStart = i;
                }
                else
                {
                    currentSum += nums[i];
                }

                // Update the maximum sum and subarray indices if the current sum is greater
                if (currentSum > maxSum)
                {
                    maxSum = currentSum;
                    start = currentStart;
                    end = i;
                }
            }

            // Construct the subarray with the maximum sum
            int[] maxSubarray = new int[end - start + 1];
            Array.Copy(nums, start, maxSubarray, 0, maxSubarray.Length);
            return maxSubarray;
        }
        public int PositiveSmallestMissingIntegerGreaterThanZero(int[] A)
        {
            int n = A.Length;
            bool[] present = new bool[n + 1];

            // Mark the presence of positive integers in the array
            for (int i = 0; i < n; i++)
            {
                if (A[i] > 0 && A[i] <= n)
                    present[A[i]] = true;
            }

            // Find the smallest positive integer that is not present in the array
            for (int i = 1; i <= n; i++)
            {
                if (!present[i])
                    return i;
            }

            // If all positive integers from 1 to n are present, return n+1
            return n + 1;
        }
    }
}
