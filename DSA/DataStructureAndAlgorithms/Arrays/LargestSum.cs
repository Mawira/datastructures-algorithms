﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSA.DataStructureAndAlgorithms.Arrays
{
    class LargestSum
    {
        public int Sum()
        {
            int[] arr = { 7,1,2,-1,3,4,10,-12,3,21,-19 };

            if (arr.Length == 0)
                return 0;
            if (arr.Length == 1)
                return arr[0];

            int currentSum = arr[0];
            int maxSum = 0;

            for (int i = 1; i < arr.Length; i++)
            {
                int tempSum = currentSum + arr[i];

                if (tempSum > currentSum)
                {
                    currentSum = tempSum;
                }
                if (maxSum < currentSum)
                {
                    maxSum = currentSum;
                }
            }
            return maxSum;
        }
    }
}
